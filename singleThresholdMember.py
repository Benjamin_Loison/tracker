#!/usr/bin/python3

# Could force the `BEGIN_TIME`.

import subprocess
from pathlib import Path

def execute(cmd):
    return subprocess.check_output(cmd, shell=True).decode('utf-8')

def printIfSomething(s):
    if s != '':
        print(s)

print('Loading last time we received data from the tracker')
cmd = './viewTimesStr.py && cat readible.txt'
res = execute(cmd)
lines = res.split('\n')
lastTime = ' '.join(lines[-1].split()[3:5])
print(f'Last time we received data from the tracker was: {lastTime}')

UTILS_FILE_NAME = 'utils.py'

with open(UTILS_FILE_NAME) as f:
    lines = f.read().splitlines()
linesLen = len(lines)

PREFIX = "END_TIME = getTimeFromStr('"

for linesIndex, line in enumerate(lines):
    if line.startswith(PREFIX):
        lines[linesIndex] = PREFIX + lastTime + "')"
        break

PREFIX = 'MY_PASSPHRASE = PASSPHRASES['
THRESHOLD_MEMBERS_COUNT = 3

Path('hashes').mkdir(exist_ok = True)
for i in range(THRESHOLD_MEMBERS_COUNT):
    print(f'Threshold decrypting {i + 1} / {THRESHOLD_MEMBERS_COUNT}')
    with open(UTILS_FILE_NAME, 'w') as f:
        for linesIndex, line in enumerate(lines):
            if line.startswith(PREFIX):
                line = PREFIX + str(i) + ']'
            f.write(line)
            if linesIndex < linesLen - 1:
                f.write('\n')
    cmd = f'rm -r __pycache__; ./thresholdDecrypt.py && mv hashes.txt hashes/{i}.txt'
    res = execute(cmd)
    printIfSomething(res)

print('Decrypting and converting to CSV')
cmd = './decrypt.py && ./latLonToCSV.py'
res = execute(cmd)
printIfSomething(res)

