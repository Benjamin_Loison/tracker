import hashlib
import requests
import time
import subprocess
import signal
from datetime import datetime

# Uses SHA512
def trackerHash(toHash):
    return hashlib.sha512(toHash.encode('utf-8')).hexdigest()

def getFromURL(url, params):
    return requests.get(url, params).text

def getTime():
    return int(time.time())

TIME_FORMAT = '%d-%m-%Y %H:%M:%S'

def getTimeFromStr(timeStr):
    return time.mktime(datetime.strptime(timeStr, TIME_FORMAT).timetuple())

def getTimeStrFromInt(timeInt):
    return datetime.fromtimestamp(timeInt).strftime(TIME_FORMAT)

def getTrack():
    for TRACK_URL in TRACK_URLS:
        params = {
            'key': READ_ONLY_PASSPHRASE
        }
        try:
            track = getFromURL(TRACK_URL, params).split('\n')
            return track
        except Exception as exception:
            print(exception)

def isEncrypted(data):
    return not (data.startswith('Battery') or data.startswith('Retrieving'))

# Assume that trigger the signal once per Python script execution.

# Register an handler for the timeout
def handler(_signum, _frame):
    raise Exception('end of time')

# Pay attention that `execute` is called multiple times per Python script execution but only once to retrieve the location and potentially not as the last call.
# This function *may* run for an indetermined time...
def execute(command, timeout = None):
    return subprocess.check_output(command, shell = True).decode('utf-8')

#Register the signal function handler
signal.signal(signal.SIGALRM, handler)

# Define a timeout for your function
signal.alarm(50)

## Constants:

ENABLE_ENCRYPTION = True

MY_PASSPHRASE = 'MY_PASSPHRASE'

PASSPHRASES = ['PASSPHRASE_A', 'PASSPHRASE_B', 'PASSPHRASE_C']

TRACKER_PASSPHRASE = 'TRACKER_PASSPHRASE'

TRACK_URLS = [
    'https://yourwebsite0.com/tracker/track.php',
    'https://yourwebsite1.com/tracker/track.php',
]

READ_ONLY_PASSPHRASE = 'READ_ONLY_PASSPHRASE'

BEGIN_TIME = getTimeFromStr('23-09-2022 15:00:00')
# For determinism, always provide as an end time a time for a message that is already received.
END_TIME = getTimeFromStr('25-09-2022 15:00:00')
