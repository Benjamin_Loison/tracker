<?php

    require 'config.php';
    require_once 'utils.php';

    $KEY_VARIABLE_ID = 'key';
    $DATA_VARIABLE_ID = 'data';

    function logg($toEcho, $toWrite)
    {
        global $TRACK_FILE_PATH;
        $toWrite = "\n" . time() . " $_SERVER[REMOTE_ADDR] $toWrite";
        file_put_contents($TRACK_FILE_PATH, $toWrite, FILE_APPEND);
        echo $toEcho;
    }

    function isTheInterestingTrackerWriting($access, $identifier)
    {
        global $INTERESTING_TRACKER;
        return $access === Access::Write && $identifier === $INTERESTING_TRACKER;
    }

    if(isset($_GET[$KEY_VARIABLE_ID]))
    {
        $keyTested = $_GET[$KEY_VARIABLE_ID];
        if(array_key_exists($keyTested, $KEYS))
        {
            [$identifier, $access] = $KEYS[$keyTested];
            $identifierLowerCase = strtolower($identifier);
            $TRACK_FILE_PATH = "$TRACKER_FOLDER_PATH/$identifierLowerCase.txt";
            if(isset($_GET[$DATA_VARIABLE_ID]))
            {
                $data = $_GET[$DATA_VARIABLE_ID];
                if(isTheInterestingTrackerWriting($access, $identifier))
                {
                    // Have to retrieve the file last modification time at this point in the code as afterwards the concerned file is modified, hence retrieving this metadata is no more relevant.
                    $lastModificationTime = filemtime($TRACK_FILE_PATH);
                }
                logg('Received', "$data");
                if(isTheInterestingTrackerWriting($access, $identifier))
                {
                    // `date -r /var/www/priv/tracker/TRACKER.txt +%s`
                    if($lastModificationTime == 1710182360)
                    {
                        $message = "$identifier tracker is on!";
                        $message = escapeshellarg($message);
                        exec("matrix-commander -m $message");
                    }
                    $data = implode(' ', array_slice(explode(' ', $data), 1));
                    $data = json_decode($data, true);
                    $latitude = $data['latitude'];
                    $longitude = $data['longitude'];
                    $distance = haversineGreatCircleDistance($latitude, $longitude, $INTERESTING_LOCATION_LATITUDE, $INTERESTING_LOCATION_LONGITUDE);
                    $distanceThreshold = 500;
                    if($distance /*<*/> $distanceThreshold)
                    {
                        $operator = /*'Less'*/'More';
                        $distanceStr = round($distance, 2);
                        $message = "$operator than $distanceThreshold meters (actually $distanceStr meters, ($latitude, $longitude)) from interesting location ($INTERESTING_LOCATION_LATITUDE, $INTERESTING_LOCATION_LONGITUDE)!";
                        $message = escapeshellarg($message);
                        // Should use a boolean.
                        //exec("matrix-commander -m $message");
                    }
                }
            }
            else
            {
                $content = file_get_contents($TRACK_FILE_PATH);
                if(!isset($_GET['last']))
                {
                    echo $content;
                }
                else
                {
                    echo `tail -n 1 $TRACK_FILE_PATH`;
                }
            }
        }
        else
        {
            echo 'Incorrect key';
        }
    }
    else
    {
        echo 'Key or data not provided';
    }

?>
