#!/usr/bin/python3

import csv, json, utils
from colour import Color

useTrackerData = False

with (open('track.txt' if useTrackerData else 'decrypted.txt')) as decryptedFile:
    lines = decryptedFile.read().splitlines()
linesLen = len(lines)

colors = list(Color('blue').range_to(Color('green'), linesLen))

with open('latLon.csv', 'w') as latLonFile:
    fieldnames = ['lat', 'lon', 'name', 'color']
    writer = csv.DictWriter(latLonFile, fieldnames=fieldnames, quoting=csv.QUOTE_NONNUMERIC)
    writer.writeheader()
    for linesIndex in range(linesLen):
        line = lines[linesIndex]
        if len(line) > 0 and line[-1] == '{':
            locationStr = '{' + ''.join(lines[linesIndex + 1:linesIndex + 11])
            location = json.loads(locationStr)
            lineParts = line.split()
            name = utils.getTimeStrFromInt(int(lineParts[0])) if useTrackerData else ' '.join(lineParts[4:6])
            color = colors[linesIndex]
            writer.writerow({'lat': location['latitude'], 'lon': location['longitude'], 'name': name, 'color': color})
            linesIndex += 10
