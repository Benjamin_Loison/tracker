#!/usr/bin/python3

import utils

track = utils.getTrack()
isFirst = True

with open('hashes.txt', 'w') as f:
    for line in track:
        lineParts = line.split()
        time = lineParts[2]
        timeInt = int(time)
        if utils.BEGIN_TIME <= timeInt and timeInt <= utils.END_TIME:
            data = lineParts[3]
            if utils.isEncrypted(data):
                hashTime = utils.trackerHash(utils.MY_PASSPHRASE + time)
                if not isFirst:
                    f.write('\n')
                isFirst = False
                f.write(hashTime)
