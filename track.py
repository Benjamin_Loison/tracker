#!/data/data/com.termux/files/usr/bin/python3

import requests
import shlex
import utils

# Uses AES 256 CBC
def encrypt(toEncrypt, passphrase):
    cmd = f"echo {shlex.quote(toEncrypt)} | openssl aes-256-cbc -pbkdf2 -a -pass {shlex.quote(f'pass:{passphrase}')}"
    return utils.execute(cmd).replace('\n', '')

# See [Benjamin-Loison/termux-api/issues/1](https://github.com/Benjamin-Loison/termux-api/issues/1).
PROVIDE_FAKE_LOCATION = False

from datetime import datetime

def getLocation():
    if PROVIDE_FAKE_LOCATION:
        return ''
    try:
        return utils.execute('termux-location')[:-1]
    except Exception as exception:
        return ''

def log(data):
    with open('track.txt', 'a') as f:
        f.write(data + '\n')

def send(data):
    data = requests.utils.quote(data)
    results = []
    for TRACK_URL in utils.TRACK_URLS:
        params = {
            'key': utils.TRACKER_PASSPHRASE,
            'data': data,
        }
        try:
            results += [utils.getFromURL(TRACK_URL, params)]
        except Exception as exception:
            print(exception)
    return results

# While `thresholdHash` may not be used in the following, as it is a cheap operation (0.1 ms) let us keep this code order, as on an abstract cryptographic level it may make to some extent more sense.
currentTime = str(utils.getTime())
print(currentTime)
hashes = [utils.trackerHash(passphrase + currentTime) for passphrase in utils.PASSPHRASES]
thresholdHash = ''.join(hashes)
print(thresholdHash)

location = getLocation()
if location != '':
    print(location)
    if utils.ENABLE_ENCRYPTION:
        locationToSend = encrypt(location, thresholdHash)
        print(locationToSend)
    else:
        locationToSend = location

    data = currentTime + ' ' + locationToSend
    log(currentTime + ' ' + location)
    send(data)
else:
    RETRIEVING_LOCATION_FAILED_ERROR = 'Retrieving location failed!'
    print(RETRIEVING_LOCATION_FAILED_ERROR)
    message = currentTime + ' ' + RETRIEVING_LOCATION_FAILED_ERROR
    log(message)
    results = send(message)
    # For debugging:
    if False:
        print('send', results)

