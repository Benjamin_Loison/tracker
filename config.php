<?php

    require_once 'utils.php';

    $KEYS = [
        'TRACKER_0_PASSPHRASE' => ['Tracker 0', Access::Write],
        'TRACKER_0_READ_ONLY_PASSPHRASE' => ['Tracker 0 read-only', Access::Read],
        'TRACKER_1_PASSPHRASE' => ['Tracker 1', Access::Write],
        'TRACKER_1_READ_ONLY_PASSPHRASE' => ['Tracker 1 read-only', Access::Read],
    ];
    $TRACKER_FOLDER_PATH = '../priv/tracker';
    $INTERESTING_LOCATION_LATITUDE = 0.12345;
    $INTERESTING_LOCATION_LONGITUDE = 0.12345;
    $INTERESTING_TRACKER = 'Tracker';
?>
