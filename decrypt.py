#!/usr/bin/python3

import utils
import shlex
from tqdm import tqdm

memberHashes = []
for i in range(3):
    with open(f'hashes/{i}.txt') as f:
        memberHashes += [f.read().splitlines()]

track = utils.getTrack()
hashesIndex = 0
isFirst = True

ENCRYPTED_FIELD = 3
TIME_FIELDS = [0, 2]

def decrypt(encrypted, passphrase):
    cmd = f'echo {shlex.quote(encrypted)} | openssl aes-256-cbc -d -pbkdf2 -a -pass {shlex.quote(f"pass:{passphrase}")}'
    return utils.execute(cmd)

with open('decrypted.txt', 'w') as f:
    for line in tqdm(track):
        lineParts = line.split()
        time = lineParts[2]
        timeInt = int(time)
        for TIME_FIELD in TIME_FIELDS:
            time = int(lineParts[TIME_FIELD])
            timeStr = utils.getTimeStrFromInt(time)
            lineParts[TIME_FIELD] = timeStr
        line = ' '.join(lineParts)
        if utils.BEGIN_TIME <= timeInt and timeInt <= utils.END_TIME:
            data = lineParts[ENCRYPTED_FIELD]
            if utils.isEncrypted(data):
                thresholdHash = ''
                for memberHash in memberHashes:
                    thresholdHash += memberHash[hashesIndex]
                decrypted = decrypt(data, thresholdHash)
                lineParts[3] = decrypted
                hashesIndex += 1
                line = ' '.join(lineParts)
            if not isFirst:
                f.write('\n')
            isFirst = False
            f.write(line)
