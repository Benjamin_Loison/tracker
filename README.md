# Tracker

Track device geolocation in a privacy friendly manner thanks to Termux and a web-server using PHP.

Dependencies:

```bash
pip install colour
```

Note that privacy properties means:
- use a 2 among 3 threshold for decryption
- threshold members can cooperate to decrypt on a message by message basis

At any moment if the battery level goes below 5 % or the geolocation is lost, then a notification of this event is sent without any privacy property.
To do so add to your crontab (`crontab -e`):
```bash
* * * * * B=$(termux-battery-status) && test $(echo $B | jq '.percentage') -lt 6 && curl --get --data-urlencode 'key=TRACKER_PASSPHRASE' --data-urlencode "data=$(date +%s) Battery level is under 5 %" https://yourwebsite.com/tracker/track.php
```

To enable the tracker add to your crontab:
```bash
* * * * * cd track/ && ./track.py
```

Constants (except for the web-server) are defined in `utils.py`.

Note that all (encrypted and not encrypted) notifications are accessible by providing a passphrase to the web-server.

Currently en/decryption scheme used is AES-256-CBC and hasing function used is SHA512.

For threshold encryption, for instance for 2 among 3 threshold, the tracker defines 3 passphrases denoted as `A`, `B` and `C`.
When encrypting the tracker uses the passphrase `ABC` and the tracker shares to each threshold decryption member a distinct pair of distinct passphrases.
For instance the tracker gives to threshold member:
- `x` the passphrases `A` and `B`
- `y` the passphrases `B` and `C`
- `z` the passphrases `A` and `C`

That way anyone of them can collaborate with another to get the full passphrase that is `ABC`.
Note that as far as I know the security relies in the weakest passsentence of `A`, `B` and `C`.
